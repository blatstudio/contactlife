class SimulationsController < ApplicationController
  def index
    @simulations = Simulation.all
  end

  def show
    @simulation = Simulation.find params[:id]
    get_siminfo()

    respond_to do |format|
      format.html
      format.pdf do
        header_html = render_to_string 'simulations/header_pdf.html', layout: nil
        footer_html = render_to_string 'simulations/footer_pdf.html', layout: nil
        render pdf: "contacto_#{@simulation.id}",
          layout: 'pdf.html', # use 'pdf.html' for a pdf.html.erb file
          footer: {content: footer_html, spacing: 0},
          margin: {top: 45, bottom: 29},
          header: {content: header_html, spacing: 5}
      end
    end
  end

  def new
    @simulation = Simulation.new
    @simulation.build_other_information
    personal = @simulation.build_personal
    personal.build_personal_auto
    personal.build_personal_health
    personal.build_personal_saving
    personal.build_personal_life
    personal.build_personal_work_accident
    personal.build_personal_responsibility
    personal.build_housing
    personal.build_boat
    small_business = @simulation.build_small_business
    small_business.build_small_auto
    small_business.build_small_person
    small_business.build_small_azard
    small_business.build_small_responsibility
    business = @simulation.build_business
    business.build_business_auto
    business.build_business_person
    business.build_business_azard
    business.build_business_civil
  end

  def create
    @sim_params = simulation_params
    @simulation = Simulation.new simulation_params
    if @simulation.save
      pdf = render_pdf
      SimulationMailer.send_pdf(@simulation, pdf).deliver
      redirect_to simulations_path
    else
      render :new
    end
  end

  def resend
    @simulation = Simulation.find params[:id]
    pdf = render_pdf
    SimulationMailer.send_pdf(@simulation, pdf).deliver
    render 'show'
  end

  private
  def render_pdf
    get_siminfo
    header_html = render_to_string 'simulations/header_pdf.html', layout: nil
    footer_html = render_to_string 'simulations/footer_pdf.html', layout: nil
    pdf = render_to_string pdf: "contacto_#{@simulation.id}",
      template: 'simulations/show.pdf.erb',
      layout: 'pdf.html', # use 'pdf.html' for a pdf.html.erb file
      footer: {content: footer_html, spacing: 0},
      margin: {top: 45, bottom: 29},
      header: {content: header_html, spacing: 5}
  end

  def get_siminfo
    if @simulation.category == 'Vida'
      @life = @simulation.life
    elsif @simulation.category == 'Particulares'
      @personal = @simulation.personal
      if @personal.category == 'Auto'
        @auto = @personal.personal_auto
      elsif @personal.category == 'Saúde'
        @health = @personal.personal_health
      end
    end
  end

  def simulation_params
    sim_params = params.require(:simulation)
    permit = [:name, :tax_id, :email, :phone, :category]
    others = {other_information_attributes: [:body]}
    permit.append others
    permit.append case sim_params[:category]
      when 'Particulares'
        personal_attributes = [:category]
        personal_attributes.append case sim_params[:personal_attributes][:category]
          when 'Auto'
            {personal_auto_attributes: [:ensurance_type, :birthday, :driver_license_date, :license_plate, :post_code] }
          when 'Saúde'
            {personal_health_attributes: [:ensurance_type, :gender, :activity_sector, :birthday] }
          when 'Habitação'
            {housing_attributes: [:realty_type, :usage, :post_code, :build_date, :rebuild_date] }
          when 'Poupança'
            {personal_saving_attributes: [:category] }
          when 'Seguros de Vida e Acidentes Pessoais'
            {personal_life_attributes: [:birthday, :gender, :career, :sector, :ensurance_type] }
          when 'Acidentes de Trabalho'
            {personal_work_accident_attributes: [:category] }
          when 'Responsabilidade Civil'
            {personal_responsibility_attributes: [:category] }
        end
        {personal_attributes: personal_attributes}

      when 'PME'
        small_business_attributes = [:category]
        small_business_attributes.append case sim_params[:small_business_attributes][:category]
          when 'Auto'
            {small_auto_attributes: [:ensurance] }
          when 'Pessoas'
            {small_person_attributes: [:category, :amount] }
          when 'Acidentes Trabalho PME'
            {small_azard_attributes: [:activity_sector, :amount] }
          when 'Património e Responsabilidades'
            {small_responsibility_attributes: [:category] }
        end
        {small_business_attributes: small_business_attributes}

      when 'Empresas'
        business_attributes = [:category]
        business_attributes.append case sim_params[:business_attributes][:category]
          when 'Auto'
            {business_auto_attributes: [:ensurance] }
          when 'Pessoas'
            {business_person_attributes: [:ensurance, :amount] }
          when 'Acidentes de Trabalho'
            {business_azard_attributes: [:activity_sector, :amount] }
          when 'Responsabilidade Civil'
            {business_civil_attributes: [:ensurance] }
        end
        {business_attributes: business_attributes}
    end

    sim_params.permit(permit)
  end
end
