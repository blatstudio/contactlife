// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require jquery.validate.localization/messages_pt_PT

$(document).ready(function() {
  $('#new_simulation').validate({
    rules: {
      'simulation[category]': "required",
      'simulation[name]': "required",
      'simulation[tax_id]': {
        required: true,
        minlength: 9,
        maxlength: 9
      },
      'simulation[email]': {
        required: true,
        email: true
      },
      'simulation[phone]': {
        required: true,
      },
      'simulation[personal_attributes][category]': "required",
      'simulation[personal_attributes][personal_auto_attributes][ensurance_type]': "required",
      'simulation[personal_attributes][personal_auto_attributes][birthday(3i)]': "required",
      'simulation[personal_attributes][personal_auto_attributes][birthday(2i)]': "required",
      'simulation[personal_attributes][personal_auto_attributes][birthday(1i)]': "required",
      'simulation[personal_attributes][personal_auto_attributes][driver_license_date(3i)]': "required",
      'simulation[personal_attributes][personal_auto_attributes][driver_license_date(2i)]': "required",
      'simulation[personal_attributes][personal_auto_attributes][driver_license_date(1i)]': "required",
      'simulation[personal_attributes][personal_auto_attributes][license_plate]': "required",
      'simulation[personal_attributes][personal_auto_attributes][post_code]': "required",
      'simulation[personal_attributes][personal_health_attributes][ensurance_type]': "required",
      'simulation[personal_attributes][personal_health_attributes][birthday(3i)]': "required",
      'simulation[personal_attributes][personal_health_attributes][birthday(2i)]': "required",
      'simulation[personal_attributes][personal_health_attributes][birthday(1i)]': "required",
      'simulation[personal_attributes][personal_health_attributes][gender]': "required",
      'simulation[personal_attributes][personal_health_attributes][activity_sector]': "required",
    },
    messages: {
      required: "",
      'simulation[personal_attributes][personal_auto_attributes][birthday(3i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_auto_attributes][birthday(2i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_auto_attributes][birthday(1i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_auto_attributes][driver_license_date(3i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_auto_attributes][driver_license_date(2i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_auto_attributes][driver_license_date(1i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_health_attributes][birthday(3i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_health_attributes][birthday(2i)]': {
        required: ""
      },
      'simulation[personal_attributes][personal_health_attributes][birthday(1i)]': {
        required: ""
      },
    }
  });

  anim = '0rem';
  parts = '#small-business-part, #main-part, #personal-part, #business-part, #other-part';
  personal_category = '#simulation_personal_attributes_category';
  small_category = '#simulation_small_business_attributes_category';
  business_category = '#simulation_business_attributes_category';

  $("#simulation_small_business_attributes_small_person_attributes_category, #simulation_business_attributes_business_person_attributes_ensurance").change( function() {
    var category = $(this).val();
    console.log(category);

    switch(category) {
      case 'Saúde':
        $("#small-business-people-amount").removeClass('hide');
        break;
      case 'Saúde Empresas':
        $("#business-people-amount").removeClass('hide');
        break;
      default:
        $("#small-business-people-amount, #business-people-amount").addClass('hide');
        break;
    }
  });

  $(small_category).change( function() {
    var business_parts = '#other-part,#small-business-auto-part, #small-business-people-part, #small-business-azards-part, #small-business-civil-part';
    var id = '';
    var category = $(this).val();
    var after_hide = true;

    switch(category) {
      case 'Auto':
        id = '#small-business-auto-part';
        break;
      case 'Pessoas':
        id = '#small-business-people-part';
        break;
      case 'Acidentes Trabalho PME':
        id = '#small-business-azards-part';
        break;
      case 'Património e Responsabilidades':
        id = '#small-business-civil-part';
        break;
      default:
        after_hide = false;
        break;
    }

    id += ',#other-part';
    hide(business_parts, after_hide, id);
  });

  $(business_category).change( function() {
    var business_parts = '#other-part,#business-auto-part, #business-people-part, #business-azards-part, #business-civil-part';
    var id = '';
    var category = $(this).val();
    var after_hide = true;

    switch(category) {
      case 'Auto':
        id = '#business-auto-part';
        break;
      case 'Pessoas':
        id = '#business-people-part';
        break;
      case 'Acidentes de Trabalho':
        id = '#business-azards-part';
        break;
      case 'Responsabilidade Civil':
        id = '#business-civil-part';
        break;
      default:
        after_hide = false;
        break;
    }

    id += ',#other-part';
    hide(business_parts, after_hide, id);
  });

  $(personal_category).change( function() {
    var personal_parts = '#other-part,#personal-responsibilities, #personal-accidents, #personal-health, #personal-housing, #personal-savings, #personal-boats, #personal-auto, #personal-life';
    var id = '';
    var ensurance = $(this).val();
    var after_hide = true;

    switch(ensurance) {
      case 'Auto':
        id = '#personal-auto';
        break;
      case 'Saúde':
        id = '#personal-health';
        break;
      case 'Habitação':
        id = '#personal-housing';
        break;
      case 'Poupança':
        id = '#personal-savings';
        break;
      case 'Embarcações':
        id = '#personal-boats';
        break;
      case 'Seguros de Vida e Acidentes Pessoais':
        id = '#personal-life';
        break;
      case 'Acidentes de Trabalho':
        id = '#personal-accidents';
        break;
      case 'Responsabilidade Civil':
        id = '#personal-responsibilities';
        break;
      default:
        after_hide = false;
        break;
    }

    id += ',#other-part';
    hide(personal_parts, after_hide, id);
  });

  $('#simulation_category').change( function() {
    category = $(this).val();
    var id = '';
    var after_hide = true;

    switch(category) {
      case 'Particulares':
        id = '#personal-part';
        show_image('personal-img');
        break;
      case 'PME':
        id = '#small-business-part';
        show_image('pme-img');
        break;
      case 'Empresas':
        id = '#business-part';
        show_image('company-img');
        break;
      default:
        show_image('main-img');
    }

    if(id == '') {
      hide(parts);
    } else {
      hide(parts, show, '#main-part, ' + id);
    }
  });

});

function hide(id, after_hide = false, after_id = null) {
  $(id).removeClass('active');

  setTimeout(function() {
    $(id).addClass('hide');
    if(after_hide) {
      show(after_id);
    } else {
      resize_image();
    }
  }, 300);
}

function show(id) {
  $(id).removeClass('hide');
  setTimeout(function() {
    $(id).addClass('active');
  }, 10);
  //$(id).animate({
    //top: '0',
    //opacity: '1'
  //}, function() {
    // Finished animation
  //});
  resize_image();
}

function resize_image() {
  if(typeof resize_image.initial_height == 'undefined') {
    resize_image.initial_height = $("#new_simulation_img").height();
  }
  var new_height = $("#simulation_form").height();
  if(new_height < resize_image.initial_height) {
    new_height = resize_image.initial_height;
  }
  $("#new_simulation_img").stop();
  $("#new_simulation_img").animate({
    height: new_height
  }, function() {
  });
}

function show_image(image) {
  $("#new_simulation_img img").removeClass("active");
  $("#" + image).addClass("active");
}
