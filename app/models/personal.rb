class Personal < ActiveRecord::Base
  has_one :personal_auto, dependent: :destroy
  has_one :personal_health, dependent: :destroy
  has_one :personal_saving, dependent: :destroy
  has_one :personal_life, dependent: :destroy
  has_one :personal_work_accident, dependent: :destroy
  has_one :personal_responsibility, dependent: :destroy
  has_one :housing, dependent: :destroy
  has_one :boat, dependent: :destroy
  accepts_nested_attributes_for :personal_responsibility, :personal_work_accident, :personal_health, :housing, :personal_saving, :boat, :personal_auto, :personal_life

  validates_presence_of :category
end
