class Business < ActiveRecord::Base
  belongs_to :simulation
  has_one :business_auto, dependent: :destroy
  has_one :business_person, dependent: :destroy
  has_one :business_azard, dependent: :destroy
  has_one :business_civil, dependent: :destroy
  accepts_nested_attributes_for :business_auto, :business_person, :business_civil, :business_azard
end
