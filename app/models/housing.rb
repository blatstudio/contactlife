class Housing < ActiveRecord::Base
  belongs_to :personal

  validates_presence_of :realty_type, :usage, :build_date, :post_code

end
