class PersonalHealth < ActiveRecord::Base
  belongs_to :personal

  validates_presence_of :birthday, :ensurance_type, :activity_sector
  validates_inclusion_of :gender, in: [true, false]

  def gender_to_s
    if gender
      return 'Masculino'
    end
    return 'Feminino'
  end

end
