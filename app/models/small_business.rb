class SmallBusiness < ActiveRecord::Base
  has_one :small_auto, dependent: :destroy
  has_one :small_person, dependent: :destroy
  has_one :small_azard, dependent: :destroy
  has_one :small_responsibility, dependent: :destroy
  belongs_to :simulation
  accepts_nested_attributes_for :small_auto, :small_person, :small_responsibility, :small_azard
end
