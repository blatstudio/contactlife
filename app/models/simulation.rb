class Simulation < ActiveRecord::Base
  has_one :personal, dependent: :destroy
  has_one :business, dependent: :destroy
  has_one :small_business, dependent: :destroy
  has_one :other_information, dependent: :destroy
  accepts_nested_attributes_for :personal, :business, :small_business, :other_information

  validates_presence_of :category, :name, :tax_id, :email, :phone
end
