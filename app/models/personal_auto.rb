class PersonalAuto < ActiveRecord::Base
  belongs_to :personal

  validates_presence_of :birthday,
    :driver_license_date,
    :license_plate,
    :post_code,
    :ensurance_type
end
