class PersonalLife < ActiveRecord::Base
  belongs_to :personal

  def gender_to_s
    if gender
      return 'Masculino'
    end
    return 'Feminino'
  end

end
