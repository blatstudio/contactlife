class SimulationMailer < ApplicationMailer

  def send_pdf(simulation, pdf)
    @simulation = simulation
    get_siminfo
    attachments["#{simulation.id}.pdf"] = pdf
    mail(to: 'geral@contactlifeseguros.com', subject: "Simulação nº #{simulation.id}")
  end

  private
  def get_siminfo
    if @simulation.category == 'Vida'
      @life = @simulation.life
    elsif @simulation.category == 'Particulares'
      @personal = @simulation.personal
      if @personal.category == 'Auto'
        @auto = @personal.personal_auto
      elsif @personal.category == 'Saúde'
        @health = @personal.personal_health
      end
    end
  end

end
