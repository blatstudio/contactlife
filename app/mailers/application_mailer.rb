class ApplicationMailer < ActionMailer::Base
  default from: "Simulação <noreply@contactlife.com>"
  layout 'mailer'
end
