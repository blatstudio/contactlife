#!/bin/bash

# start a new session
tmux new-session -d -s contactlife -n editor
tmux new-window -t contactlife:2 -n server
tmux new-window -t contactlife:3 -n console

# Execute commands
tmux send-keys -t contactlife:1 'vim' enter
tmux send-keys -t contactlife:2 'rails server -b 0.0.0.0 -p 4321' enter

tmux attach -t contactlife
