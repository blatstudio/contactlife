class CreateAutos < ActiveRecord::Migration
  def change
    create_table :autos do |t|
      t.date :birthday
      t.date :driver_licence_date
      t.string :licence_place
      t.string :post_code

      t.timestamps null: false
    end
  end
end
