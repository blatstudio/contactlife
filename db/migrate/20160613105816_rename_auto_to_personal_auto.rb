class RenameAutoToPersonalAuto < ActiveRecord::Migration
  def change
    rename_table :autos, :personal_autos
  end
end
