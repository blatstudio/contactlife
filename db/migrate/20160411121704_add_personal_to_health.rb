class AddPersonalToHealth < ActiveRecord::Migration
  def change
    add_reference :healths, :personal, index: true, foreign_key: true
  end
end
