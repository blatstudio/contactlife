class CreateSimulations < ActiveRecord::Migration
  def change
    create_table :simulations do |t|
      t.string :tax_id
      t.string :category
      t.string :name
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
  end
end
