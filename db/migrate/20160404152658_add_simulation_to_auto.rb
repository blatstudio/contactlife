class AddSimulationToAuto < ActiveRecord::Migration
  def change
    add_reference :autos, :simulation, index: true, foreign_key: true
  end
end
