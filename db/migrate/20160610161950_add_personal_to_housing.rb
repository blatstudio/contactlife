class AddPersonalToHousing < ActiveRecord::Migration
  def change
    add_reference :housings, :personal, index: true, foreign_key: true
  end
end
