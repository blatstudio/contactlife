class RenameTypeInLifeToEnsuranceType < ActiveRecord::Migration
  def change
    rename_column :lives, :type, :ensurance_type
  end
end
