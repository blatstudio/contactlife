class CreateBusinessAzards < ActiveRecord::Migration
  def change
    create_table :business_azards do |t|
      t.integer :amount
      t.string :activity_sector
      t.references :business, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
