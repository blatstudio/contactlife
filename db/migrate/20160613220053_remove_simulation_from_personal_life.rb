class RemoveSimulationFromPersonalLife < ActiveRecord::Migration
  def change
    remove_reference :personal_lives, :simulation, index: true, foreign_key: true
  end
end
