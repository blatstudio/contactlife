class CreateBoats < ActiveRecord::Migration
  def change
    create_table :boats do |t|
      t.string :mode
      t.string :boat_type
      t.date :build_date
      t.string :usage

      t.timestamps null: false
    end
  end
end
