class RenameTypeInAutoToEnsuranceType < ActiveRecord::Migration
  def change
    rename_column :autos, :type, :ensurance_type
  end
end
