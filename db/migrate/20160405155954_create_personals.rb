class CreatePersonals < ActiveRecord::Migration
  def change
    create_table :personals do |t|
      t.string :category
      t.references :health, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
