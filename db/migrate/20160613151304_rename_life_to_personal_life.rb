class RenameLifeToPersonalLife < ActiveRecord::Migration
  def change
    rename_table :lives, :personal_lives
  end
end
