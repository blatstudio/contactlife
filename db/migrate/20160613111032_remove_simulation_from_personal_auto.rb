class RemoveSimulationFromPersonalAuto < ActiveRecord::Migration
  def change
    remove_reference :personal_autos, :simulation, index: true, foreign_key: true
  end
end
