class RemoveHealthFromPersonal < ActiveRecord::Migration
  def change
    remove_reference :personals, :health, index: true, foreign_key: true
  end
end
