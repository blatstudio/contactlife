class CreateOtherInformations < ActiveRecord::Migration
  def change
    create_table :other_informations do |t|
      t.text :body
      t.references :simulation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
