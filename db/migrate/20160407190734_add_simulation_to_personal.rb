class AddSimulationToPersonal < ActiveRecord::Migration
  def change
    add_reference :personals, :simulation, index: true, foreign_key: true
  end
end
