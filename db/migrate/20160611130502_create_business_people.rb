class CreateBusinessPeople < ActiveRecord::Migration
  def change
    create_table :business_people do |t|
      t.references :business, index: true, foreign_key: true
      t.string :ensurance

      t.timestamps null: false
    end
  end
end
