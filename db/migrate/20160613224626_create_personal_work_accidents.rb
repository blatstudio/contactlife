class CreatePersonalWorkAccidents < ActiveRecord::Migration
  def change
    create_table :personal_work_accidents do |t|
      t.references :personal, index: true, foreign_key: true
      t.string :category

      t.timestamps null: false
    end
  end
end
