class CreateSmallResponsibilities < ActiveRecord::Migration
  def change
    create_table :small_responsibilities do |t|
      t.references :small_business, index: true, foreign_key: true
      t.string :category

      t.timestamps null: false
    end
  end
end
