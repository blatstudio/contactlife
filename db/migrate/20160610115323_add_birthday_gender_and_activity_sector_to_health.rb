class AddBirthdayGenderAndActivitySectorToHealth < ActiveRecord::Migration
  def change
    add_column :healths, :gender, :boolean
    add_column :healths, :activity_sector, :string
  end
end
