class CreateHousings < ActiveRecord::Migration
  def change
    create_table :housings do |t|
      t.string :realty_type
      t.string :usage
      t.string :post_code
      t.date :build_date
      t.date :rebuild_date

      t.timestamps null: false
    end
  end
end
