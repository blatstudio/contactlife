class RenameHealthToPersonalHealth < ActiveRecord::Migration
  def change
    rename_table :healths, :personal_healths
  end
end
