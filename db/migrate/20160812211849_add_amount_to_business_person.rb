class AddAmountToBusinessPerson < ActiveRecord::Migration
  def change
    add_column :business_people, :amount, :integer
  end
end
