class AddTypeToAuto < ActiveRecord::Migration
  def change
    add_column :autos, :type, :string
  end
end
