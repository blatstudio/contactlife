class AddAmountToSmallPerson < ActiveRecord::Migration
  def change
    add_column :small_people, :amount, :integer
  end
end
