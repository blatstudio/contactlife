class CreateLives < ActiveRecord::Migration
  def change
    create_table :lives do |t|
      t.date :birthday
      t.boolean :gender
      t.string :career
      t.string :sector
      t.string :post_code
      t.references :simulation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
