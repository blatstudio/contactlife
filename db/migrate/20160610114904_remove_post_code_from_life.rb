class RemovePostCodeFromLife < ActiveRecord::Migration
  def change
    remove_column :lives, :post_code, :string
  end
end
