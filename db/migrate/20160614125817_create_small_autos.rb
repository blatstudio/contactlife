class CreateSmallAutos < ActiveRecord::Migration
  def change
    create_table :small_autos do |t|
      t.references :small_business, index: true, foreign_key: true
      t.string :ensurance

      t.timestamps null: false
    end
  end
end
