class AddPersonalToBoat < ActiveRecord::Migration
  def change
    add_reference :boats, :personal, index: true, foreign_key: true
  end
end
