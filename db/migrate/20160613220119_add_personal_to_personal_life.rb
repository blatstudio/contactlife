class AddPersonalToPersonalLife < ActiveRecord::Migration
  def change
    add_reference :personal_lives, :personal, index: true, foreign_key: true
  end
end
