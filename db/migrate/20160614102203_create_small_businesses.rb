class CreateSmallBusinesses < ActiveRecord::Migration
  def change
    create_table :small_businesses do |t|
      t.references :simulation, index: true, foreign_key: true
      t.string :category

      t.timestamps null: false
    end
  end
end
