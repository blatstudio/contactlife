class RenameLicenceTolicense < ActiveRecord::Migration
  def change
    rename_column :autos, :driver_licence_date, :driver_license_date
    rename_column :autos, :licence_place, :license_plate
  end
end
