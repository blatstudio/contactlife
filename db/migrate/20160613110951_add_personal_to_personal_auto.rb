class AddPersonalToPersonalAuto < ActiveRecord::Migration
  def change
    add_reference :personal_autos, :personal, index: true, foreign_key: true
  end
end
