class CreateSmallAzards < ActiveRecord::Migration
  def change
    create_table :small_azards do |t|
      t.integer :amount
      t.string :activity_sector
      t.references :small_business, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
