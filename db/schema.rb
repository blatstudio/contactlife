# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160813113012) do

  create_table "boats", force: :cascade do |t|
    t.string   "mode"
    t.string   "boat_type"
    t.date     "build_date"
    t.string   "usage"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "personal_id"
  end

  add_index "boats", ["personal_id"], name: "index_boats_on_personal_id"

  create_table "business_autos", force: :cascade do |t|
    t.integer  "business_id"
    t.string   "ensurance"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "business_autos", ["business_id"], name: "index_business_autos_on_business_id"

  create_table "business_azards", force: :cascade do |t|
    t.integer  "amount"
    t.string   "activity_sector"
    t.integer  "business_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "business_azards", ["business_id"], name: "index_business_azards_on_business_id"

  create_table "business_civils", force: :cascade do |t|
    t.integer  "business_id"
    t.string   "ensurance"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "business_civils", ["business_id"], name: "index_business_civils_on_business_id"

  create_table "business_people", force: :cascade do |t|
    t.integer  "business_id"
    t.string   "ensurance"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "amount"
  end

  add_index "business_people", ["business_id"], name: "index_business_people_on_business_id"

  create_table "businesses", force: :cascade do |t|
    t.string   "category"
    t.integer  "simulation_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "businesses", ["simulation_id"], name: "index_businesses_on_simulation_id"

  create_table "housings", force: :cascade do |t|
    t.string   "realty_type"
    t.string   "usage"
    t.string   "post_code"
    t.date     "build_date"
    t.date     "rebuild_date"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "personal_id"
    t.string   "ensurance_type"
  end

  add_index "housings", ["personal_id"], name: "index_housings_on_personal_id"

  create_table "other_informations", force: :cascade do |t|
    t.text     "body"
    t.integer  "simulation_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "other_informations", ["simulation_id"], name: "index_other_informations_on_simulation_id"

  create_table "personal_autos", force: :cascade do |t|
    t.date     "birthday"
    t.date     "driver_license_date"
    t.string   "license_plate"
    t.string   "post_code"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "ensurance_type"
    t.integer  "personal_id"
  end

  add_index "personal_autos", ["personal_id"], name: "index_personal_autos_on_personal_id"

  create_table "personal_healths", force: :cascade do |t|
    t.date     "birthday"
    t.string   "ensurance_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "personal_id"
    t.boolean  "gender"
    t.string   "activity_sector"
  end

  add_index "personal_healths", ["personal_id"], name: "index_personal_healths_on_personal_id"

  create_table "personal_lives", force: :cascade do |t|
    t.date     "birthday"
    t.boolean  "gender"
    t.string   "career"
    t.string   "sector"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "ensurance_type"
    t.integer  "personal_id"
  end

  add_index "personal_lives", ["personal_id"], name: "index_personal_lives_on_personal_id"

  create_table "personal_responsibilities", force: :cascade do |t|
    t.integer  "personal_id"
    t.string   "category"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "personal_responsibilities", ["personal_id"], name: "index_personal_responsibilities_on_personal_id"

  create_table "personal_savingls", force: :cascade do |t|
    t.integer  "business_id"
    t.string   "ensurance"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "personal_savingls", ["business_id"], name: "index_personal_savingls_on_business_id"

  create_table "personal_savings", force: :cascade do |t|
    t.integer  "personal_id"
    t.string   "category"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "personal_savings", ["personal_id"], name: "index_personal_savings_on_personal_id"

  create_table "personal_work_accidents", force: :cascade do |t|
    t.integer  "personal_id"
    t.string   "category"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "personal_work_accidents", ["personal_id"], name: "index_personal_work_accidents_on_personal_id"

  create_table "personals", force: :cascade do |t|
    t.string   "category"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "simulation_id"
  end

  add_index "personals", ["simulation_id"], name: "index_personals_on_simulation_id"

  create_table "simulations", force: :cascade do |t|
    t.string   "tax_id"
    t.string   "category"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "small_autos", force: :cascade do |t|
    t.integer  "small_business_id"
    t.string   "ensurance"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "small_autos", ["small_business_id"], name: "index_small_autos_on_small_business_id"

  create_table "small_azards", force: :cascade do |t|
    t.integer  "amount"
    t.string   "activity_sector"
    t.integer  "small_business_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "small_azards", ["small_business_id"], name: "index_small_azards_on_small_business_id"

  create_table "small_businesses", force: :cascade do |t|
    t.integer  "simulation_id"
    t.string   "category"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "small_businesses", ["simulation_id"], name: "index_small_businesses_on_simulation_id"

  create_table "small_people", force: :cascade do |t|
    t.integer  "small_business_id"
    t.string   "category"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "amount"
  end

  add_index "small_people", ["small_business_id"], name: "index_small_people_on_small_business_id"

  create_table "small_responsibilities", force: :cascade do |t|
    t.integer  "small_business_id"
    t.string   "category"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "small_responsibilities", ["small_business_id"], name: "index_small_responsibilities_on_small_business_id"

end
